//Soal 1
var a=2;
console.log("LOOPING PERTAMA");
while(a<=20){
	console.log(a+" - I love coding");
a=a+2;}

var a=20;
console.log("\nLOOPING KEDUA");
while(a>=2){
	console.log(a+" - I will become a mobile developer");
a=a-2;}
//---------------------------------
//Soal 2
var a;
console.log("\n");
for(a=1;a<=20;a++){
	if (a % 2 == 0){ 
		console.log(a+" - Berkualitas"); 
	}else if(a % 3 == 0 && a % 2 != 0) {
		console.log(a+" - I Love Coding");
	}else {
		console.log(a+" - Santai");
	}
}
//---------------------------------
//Soal 3
console.log("\n");
for($a=1;$a<=4;$a++){
	console.log("########");
}
//---------------------------------
//Soal 4
console.log("\n");
var x="#";
var xx="";
for(var a=1;a<=7;a++){
	xx=xx+x;
	console.log(xx);
}
//---------------------------------
//Soal 5
console.log("\n");
var b;
for(b=1;b<=8;b++){
	if (b % 2 != 0){ //Kondisi
		console.log(" # # # #"); //Kondisi true
	}else {
		console.log("# # # #"); //Kondisi false
	}
}